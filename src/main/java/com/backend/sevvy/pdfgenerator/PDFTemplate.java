package com.backend.sevvy.pdfgenerator;

import com.backend.sevvy.database.model.KegiatanSukarela;
import com.backend.sevvy.database.model.Pencapaian;
import com.backend.sevvy.database.model.Pendidikan;
import com.backend.sevvy.database.model.Pengalaman;
import com.backend.sevvy.database.model.Users;

import java.awt.Color;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

import org.apache.pdfbox.io.IOUtils;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.font.PDType1Font;



public abstract  class PDFTemplate {
    protected Users users;
    protected int colorCode;
    public TitleLanguage titleLang;
    public PDDocument document;
    public ByteArrayOutputStream baos;
    public PDPage page;
    public PDPageContentStream cs;
    protected PDType1Font font, fontBold;
    protected float lineHeight = 18, titleHeight = 35, subtitleHeight = 20;
    protected float pageWidth, pageHeight;
    public float xPointer, yPointer;
    protected float indent;
    protected float headerFontSize, titleFontSize, bodyFontSize;
    public Color headerFontColor, titleFontColor, bodyFontColor;
    protected int maxTextLength;

    public PDFTemplate(Users users, int colorCode, String language) {
        this.users = users;
        this.colorCode = colorCode;
        setTitleLang(language);
    }

    protected void setTitleLang(String language) {
        if (language.equals("IND")) {
            titleLang = new IndonesianTitle();
        } else if (language.equals("ENG")) {
            titleLang = new EnglishTitle();
        }
    }

    public byte[] generateCV() throws IOException {
        createCVDocument();
        setupPage();
        writeCV();
        return closeAndSaveDocument();
    }

    public void createCVDocument() throws IOException {
        document = new PDDocument();
        baos = new ByteArrayOutputStream();
        page = new PDPage(PDRectangle.A4);
        document.addPage(page);
        cs = new PDPageContentStream(document,page);
    }

    public byte[] closeAndSaveDocument() throws IOException {
        cs.close();
        document.save(baos);
        document.close();

        InputStream inputStream = new ByteArrayInputStream(baos.toByteArray());
        byte[] bytes = IOUtils.toByteArray(inputStream);
        return bytes;
    }

    public void setMaxTextLength(int maxTextLength) {
        this.maxTextLength = maxTextLength;
    }

    public void setIndent(float indent) {
        this.indent = indent;
    }

    public void resetXPointer() {
        this.xPointer = 0;
    }

    public void moveXPointer(float tx) {
        this.xPointer += tx;
    }

    public void moveXYPointer(float tx, float ty) {
        this.xPointer += tx;
        this.yPointer += ty;
    }

    public void resetYPointer() {
        this.yPointer = 0;
    }

    public void moveYPointer(float ty) {
        this.yPointer += ty;
    }

    public void writePengalaman() throws IOException {
        if (!users.getPengalamanSet().isEmpty()) {
            cs.beginText();
            moveYPointer(-35);
            cs.newLineAtOffset(xPointer, yPointer);
            cs.setFont(fontBold, titleFontSize);
            cs.setNonStrokingColor(titleFontColor);
            cs.showText(titleLang.titlePengalaman());
            cs.newLineAtOffset(0, -lineHeight);
            moveYPointer(-lineHeight);
            cs.setNonStrokingColor(bodyFontColor);
            cs.newLineAtOffset(indent, 0);
            for (Pengalaman pengalaman : users.getPengalamanSet()) {
                cs.newLineAtOffset(0, -5);
                moveYPointer(-5);
                cs.setFont(fontBold, bodyFontSize);
                cs.showText(pengalaman.getPosisi());
                cs.showText(" ("
                        + pengalaman.getPeriodeMulai()
                        + " - "
                        + pengalaman.getPeriodeSelesai() + ")");
                cs.newLineAtOffset(0, -lineHeight);
                moveYPointer(-lineHeight);
                cs.setFont(font, bodyFontSize);
                cs.showText(pengalaman.getPerusahaan());
                cs.newLineAtOffset(0, -lineHeight);
                moveYPointer(-lineHeight);
                wrapText(pengalaman.getDeskripsi(), maxTextLength);
            }
            cs.newLineAtOffset(-indent, 0);
            cs.endText();
        }
    }

    public void writePendidikan() throws IOException {
        if (!users.getPendidikanSet().isEmpty()) {
            cs.beginText();
            cs.newLineAtOffset(xPointer, yPointer);
            cs.newLineAtOffset(0, -35);
            moveYPointer(-35);
            cs.setFont(fontBold, titleFontSize);
            cs.setNonStrokingColor(titleFontColor);
            cs.showText(titleLang.titlePendidikan());

            cs.setNonStrokingColor(bodyFontColor);
            cs.newLineAtOffset(0, -20);
            moveYPointer(-20);
            cs.newLineAtOffset(indent, 0);
            for (Pendidikan pendidikan : users.getPendidikanSet()) {
                cs.setFont(fontBold, bodyFontSize);
                wrapText(pendidikan.getJenjang() + ", " + pendidikan.getJurusan(),maxTextLength);
                cs.newLineAtOffset(0, -3);
                moveYPointer(-3);
                cs.setFont(font, bodyFontSize);
                cs.showText(pendidikan.getNamaInstitusi());
                cs.newLineAtOffset(0, -lineHeight);
                moveYPointer(-lineHeight);
                cs.showText(pendidikan.getTahunMulai() + " - " + pendidikan.getTahunSelesai());
                cs.newLineAtOffset(0, -lineHeight);
                moveYPointer(-lineHeight);
            }
            cs.newLineAtOffset(-indent, 0);
            cs.endText();
        }
    }

    public void writePencapaian() throws IOException {
        if (!users.getPencapaianSet().isEmpty()) {
            cs.beginText();
            cs.newLineAtOffset(xPointer, yPointer);

            cs.newLineAtOffset(0, -35);
            moveYPointer(-35);
            cs.setFont(fontBold, titleFontSize);
            cs.setNonStrokingColor(titleFontColor);
            cs.showText(titleLang.titlePencapaian());
            cs.newLineAtOffset(0, -lineHeight);
            moveYPointer(-lineHeight);

            cs.setNonStrokingColor(bodyFontColor);
            cs.newLineAtOffset(indent, 0);
            for (Pencapaian pencapaian : users.getPencapaianSet()) {
                cs.newLineAtOffset(0, -5);
                moveYPointer(-5);
                cs.setFont(fontBold, bodyFontSize);
                cs.showText(pencapaian.getTitle());
                cs.showText(" (" + pencapaian.getTahun() + ")");
                cs.newLineAtOffset(0, -lineHeight);
                moveYPointer(-lineHeight);
                cs.setFont(font.TIMES_ROMAN, bodyFontSize);
                cs.showText(pencapaian.getInstitusi());
                cs.newLineAtOffset(0, -lineHeight);
                moveYPointer(-lineHeight);
                wrapText(pencapaian.getDeskripsi(), maxTextLength);
            }
            cs.newLineAtOffset(-indent, 0);
            cs.endText();
        }
    }

    public void writeKegiatanSukarela() throws IOException {
        if (!users.getKegiatanSukarelaSet().isEmpty()) {
            cs.beginText();
            cs.newLineAtOffset(xPointer, yPointer);

            cs.newLineAtOffset(0, -35);
            moveYPointer(-35);
            cs.setFont(fontBold, titleFontSize);
            cs.setNonStrokingColor(titleFontColor);
            cs.showText(titleLang.titleKegiatanSukarela());
            cs.newLineAtOffset(0, -lineHeight);
            moveYPointer(-lineHeight);

            cs.setNonStrokingColor(bodyFontColor);
            cs.newLineAtOffset(indent, 0);
            for (KegiatanSukarela kegiatanSukarela : users.getKegiatanSukarelaSet()) {
                cs.newLineAtOffset(0, -5);
                moveYPointer(-5);
                cs.setFont(fontBold, bodyFontSize);
                cs.showText(kegiatanSukarela.getPosisi());
                cs.showText(" ("
                        + kegiatanSukarela.getTahunMulai()
                        + " - "
                        + kegiatanSukarela.getTahunSelesai() + ")");
                cs.newLineAtOffset(0, -lineHeight);
                moveYPointer(-lineHeight);
                cs.setFont(font, bodyFontSize);
                cs.showText(kegiatanSukarela.getPerusahaan());
                cs.newLineAtOffset(0, -lineHeight);
                moveYPointer(-lineHeight);
                wrapText(kegiatanSukarela.getDeskripsi(), maxTextLength);
            }
            cs.newLineAtOffset(-indent, 0);
            cs.endText();
        }
    }

    public void wrapText(String text, int maxLength) throws IOException {
        String [] tmpText = splitString(text, maxLength);
        for (int k = 0;k < tmpText.length;k++) {
            if (!tmpText[k].equals("")) {
                cs.showText(tmpText[k]);
                cs.newLineAtOffset(0, -15);
                moveYPointer(-15);
            }
        }
    }

    //    https://stackoverflow.com/a/24060879
    public String [] splitString(String text, int maxLength) {
        int linebreaks = text.length() / maxLength;
        String [] splitText = new String[linebreaks + 1];
        String tmpText = text;
        String [] parts = tmpText.split(" ");
        StringBuilder [] stringBuilder = new StringBuilder[linebreaks + 1];
        int i = 0; //initialize counter
        int totalTextLength = 0;
        for (int k = 0; k < linebreaks + 1; k++) {
            stringBuilder[k] = new StringBuilder();
            while (true) {
                if (i >= parts.length) {
                    break;
                }
                totalTextLength = totalTextLength + parts[i].length();
                if (totalTextLength > maxLength) {
                    break;
                }
                stringBuilder[k].append(parts[i]);
                stringBuilder[k].append(" ");
                i++;
            }
            totalTextLength = 0;
            splitText[k] = stringBuilder[k].toString();
        }
        return splitText;
    }

    public void drawSkillBar(int value, float startX, float startY) throws IOException {
        int filledBar = value / 20;
        float currentX = startX;
        float currentY = startY;;
        for (int i = 0; i < filledBar; i++) {
            cs.setNonStrokingColor(new Color(76, 74, 85));
            cs.addRect(currentX, currentY, 15, 8);
            currentX += 17;
            cs.fill();
        }

        for (int i = filledBar; i < 5; i++) {
            cs.setNonStrokingColor(Color.WHITE);
            cs.addRect(currentX, currentY, 15, 8);
            currentX += 17;
            cs.fill();
        }
    }

    public abstract void setupPage() throws IOException;

    public abstract void writeCV() throws IOException;

    public abstract void writeName() throws IOException;

    public abstract void writePersonalInfo() throws IOException;

    public abstract void writeSitus() throws IOException;

    public abstract void writeKeterampilan() throws IOException;

    public abstract void setDocumentFont();

    public abstract void setDocumentColor(int colorCode);

}
