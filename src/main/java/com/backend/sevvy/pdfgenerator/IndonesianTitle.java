package com.backend.sevvy.pdfgenerator;

public class IndonesianTitle implements TitleLanguage {
    @Override
    public String titlePersonalInfo() {
        return "Informasi Diri";
    }

    @Override
    public String titleSitus() {
        return "Media Sosial";
    }

    @Override
    public String titlePengalaman() {
        return "Pengalaman";
    }

    @Override
    public String titlePendidikan() {
        return "Pendidikan";
    }

    @Override
    public String titlePencapaian() {
        return "Pencapaian";
    }

    @Override
    public String titleKegiatanSukarela() {
        return "Kegiatan Sukarela";
    }

    @Override
    public String titleKeterampilan() {
        return "Keterampilan";
    }
}
