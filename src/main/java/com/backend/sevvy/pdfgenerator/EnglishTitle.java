package com.backend.sevvy.pdfgenerator;

public class EnglishTitle implements TitleLanguage {
    @Override
    public String titlePersonalInfo() {
        return "Personal Information";
    }

    @Override
    public String titleSitus() {
        return "Social Media";
    }

    @Override
    public String titlePengalaman() {
        return "Work Experience";
    }

    @Override
    public String titlePendidikan() {
        return "Education";
    }

    @Override
    public String titlePencapaian() {
        return "Achievements";
    }

    @Override
    public String titleKegiatanSukarela() {
        return "Volunteer";
    }

    @Override
    public String titleKeterampilan() {
        return "Skills";
    }
}
