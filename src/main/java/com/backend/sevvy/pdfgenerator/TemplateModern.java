package com.backend.sevvy.pdfgenerator;

import com.backend.sevvy.database.model.Keterampilan;
import com.backend.sevvy.database.model.Situs;
import com.backend.sevvy.database.model.Users;
import java.awt.Color;
import java.io.IOException;
import org.apache.pdfbox.pdmodel.font.PDType1Font;



public class TemplateModern extends PDFTemplate {

    final static Color COLOR1_HEADER = new Color(76, 74, 85);
    final static Color COLOR1_ACCENT = new Color(251, 254, 251);
    final static Color COLOR1_BG = new Color(255, 255, 255);
    final static Color COLOR2_HEADER = new Color(45, 48, 71);
    final static Color COLOR2_ACCENT = new Color(250, 201, 184);
    final static Color COLOR2_BG = new Color(251, 245, 243);
    public Color headerColor;
    public Color accentColor;
    public Color bgColor;
    int headerHeight = 102;
    int leftSectionWidth = 263;
    float marginLeftHeader = 45;
    float marginTopHeader = 50;
    float marginLeft1 = 32;
    float marginLeft2 = 23;

    public TemplateModern(Users users, int colorCode, String titleLang) {
        super(users, colorCode, titleLang);
        setIndent(0);
        setDocumentColor(colorCode);
        setDocumentFont();
    }

    @Override
    public void setupPage() throws IOException {
        setMaxTextLength(80);
        pageWidth = page.getMediaBox().getWidth();
        pageHeight = page.getMediaBox().getHeight();

        cs.setNonStrokingColor(bgColor);
        cs.addRect(0, 0, pageWidth, pageHeight);
        cs.fill();

        cs.setNonStrokingColor(accentColor);
        cs.addRect(0, 0, leftSectionWidth, pageHeight);
        cs.fill();

        cs.setNonStrokingColor(headerColor);
        cs.addRect(0, pageHeight - headerHeight,
                pageWidth, headerHeight);
        cs.fill();

        headerFontSize = 28;
        bodyFontSize = 12;
        titleFontSize = 18;

        setMaxTextLength(38);
    }

    @Override
    public void writeCV() throws IOException {
        writeName();
        writePersonalInfo();
        writePendidikan();
        writeKeterampilan();
        writeSitus();

        resetXPointer();
        resetYPointer();
        moveXYPointer(leftSectionWidth + marginLeft2, pageHeight - headerHeight);
        setMaxTextLength(47);
        writePengalaman();
        writeKegiatanSukarela();
        writePencapaian();
    }

    @Override
    public void writeName() throws IOException {
        cs.beginText();
        cs.setFont(fontBold, headerFontSize);
        cs.setNonStrokingColor(headerFontColor);
        moveXYPointer(marginLeftHeader, pageHeight - marginTopHeader);
        cs.newLineAtOffset(xPointer, yPointer);
        cs.showText(users.getPersonalInfo().getFirstName() + " ");
        cs.setFont(font, headerFontSize);
        cs.showText(users.getPersonalInfo().getLastName());
        cs.endText();
    }

    @Override
    public void writePersonalInfo() throws IOException {
        cs.beginText();
        cs.newLineAtOffset(xPointer, yPointer);
        cs.newLineAtOffset(0, -20);
        cs.setFont(PDType1Font.TIMES_ROMAN, bodyFontSize);
        cs.showText(users.getPersonalInfo().getEmail() + " | "
                + users.getPersonalInfo().getPhoneNumber());
        cs.endText();

        cs.beginText();
        resetXPointer();
        resetYPointer();;
        moveXYPointer(marginLeft1, pageHeight - headerHeight);
        moveYPointer(-35);
        cs.setNonStrokingColor(titleFontColor);
        cs.setFont(fontBold, titleFontSize);
        cs.newLineAtOffset(xPointer, yPointer);
        cs.showText(titleLang.titlePersonalInfo());

        cs.setNonStrokingColor(bodyFontColor);
        cs.setFont(font, bodyFontSize);
        cs.newLineAtOffset(0, -20);
        moveYPointer(-20);
        wrapText(users.getPersonalInfo().getAddress() + ", "
                + users.getPersonalInfo().getCity(), maxTextLength);
        moveYPointer(-15);
        cs.showText(users.getPersonalInfo().getProvince());
        cs.newLineAtOffset(0, -15);
        moveYPointer(-15);
        cs.showText(users.getPersonalInfo().getPostalCode());
        cs.endText();
    }


    @Override
    public void writeKeterampilan() throws IOException {
        if (!users.getKeterampilanSet().isEmpty()) {
            cs.beginText();
            cs.newLineAtOffset(xPointer, yPointer);
            cs.newLineAtOffset(0, -35);
            moveYPointer(-35);
            cs.setFont(PDType1Font.TIMES_BOLD, titleFontSize);
            cs.setNonStrokingColor(titleFontColor);
            cs.showText(titleLang.titleKeterampilan());

            cs.setFont(PDType1Font.TIMES_ROMAN, bodyFontSize);
            cs.setNonStrokingColor(bodyFontColor);
            cs.newLineAtOffset(0, -20);
            moveYPointer(-20);
            for (Keterampilan keterampilan : users.getKeterampilanSet()) {
                cs.showText(keterampilan.getNamaKeterampilan());
                cs.newLineAtOffset(0, -18);
            }
            cs.endText();

            float currentY = yPointer;
            for (Keterampilan keterampilan : users.getKeterampilanSet()) {
                drawSkillBar(keterampilan.getTingkatKeterampilan(), 130, currentY);
                moveYPointer(-18);
                currentY = yPointer;
            }

        }
    }

    @Override
    public void writeSitus() throws IOException {
        if (!users.getSitusSet().isEmpty()) {
            cs.beginText();
            cs.newLineAtOffset(xPointer, yPointer - 32);
            cs.setFont(PDType1Font.TIMES_BOLD, titleFontSize);
            cs.setNonStrokingColor(titleFontColor);
            cs.showText(titleLang.titleSitus());

            cs.setFont(PDType1Font.TIMES_ROMAN, bodyFontSize);
            cs.setNonStrokingColor(bodyFontColor);
            cs.newLineAtOffset(0, -20);
            moveYPointer(-20);
            for (Situs situs : users.getSitusSet()) {
                cs.showText(situs.getLabel());
                cs.newLineAtOffset(100 - marginLeft1, 0);
                cs.showText(situs.getLink());
                cs.newLineAtOffset(-100 + marginLeft1, -15);
                moveYPointer(-15);
            }
            cs.endText();
        }
    }

    @Override
    public void setDocumentFont() {
        headerFontSize = 28;
        bodyFontSize = 12;
        titleFontSize = 18;
        font = PDType1Font.TIMES_ROMAN;
        fontBold = PDType1Font.TIMES_BOLD;
    }

    @Override
    public void setDocumentColor(int colorCode) {
        this.headerFontColor = COLOR1_ACCENT;
        this.titleFontColor = COLOR1_HEADER;
        this.bodyFontColor = Color.BLACK;

        switch (colorCode) {
            case 1:
                this.headerColor = COLOR1_HEADER;
                this.accentColor = COLOR1_ACCENT;
                this.bgColor = COLOR1_BG;
                break;
            case 2:
                this.headerColor = COLOR2_HEADER;
                this.accentColor = COLOR2_ACCENT;
                this.bgColor = COLOR2_BG;
                break;
        }
    }
}
