package com.backend.sevvy.database.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table
public class Pendidikan implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;

    @Column(name = "jenjang")
    private String jenjang;

    @Column(name = "namaInstitusi")
    private String namaInstitusi;

    @Column(name = "jurusan")
    private String jurusan;

    @Column(name = "tahunMulai", nullable = false)
    private int tahunMulai;

    @Column(name = "tahunSelesai")
    private int tahunSelesai;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(referencedColumnName = "email", updatable = false)
    private Users users;

    public Pendidikan() {

    }

    public Pendidikan(Long id, String jenjang, String namaInstitusi, String jurusan,
                      int tahunMulai, int tahunSelesai) {
        this.id = id;
        this.jenjang = jenjang;
        this.namaInstitusi = namaInstitusi;
        this.jurusan = jurusan;
        this.tahunMulai = tahunMulai;
        this.tahunSelesai = tahunSelesai;
    }

    public String getJenjang() {
        return jenjang;
    }

    public String getNamaInstitusi() {
        return namaInstitusi;
    }

    public String getJurusan() {
        return jurusan;
    }

    public int getTahunMulai() {
        return tahunMulai;
    }

    public int getTahunSelesai() {
        return tahunSelesai;
    }

    public Pendidikan setNamaInstitusi(String namaInstitusi) {
        this.namaInstitusi = namaInstitusi;
        return this;
    }

    public Pendidikan setJenjang(String jenjang) {
        this.jenjang = jenjang;
        return this;
    }

    public Pendidikan setJurusan(String jurusan) {
        this.jurusan = jurusan;
        return this;
    }

    public Pendidikan setTahunMulai(int tahunMulai) {
        this.tahunMulai = tahunMulai;
        return this;

    }

    public Pendidikan setTahunSelesai(int tahunSelesai) {
        this.tahunSelesai = tahunSelesai;
        return this;
    }

    public void setUsers(Users users) {
        this.users = users;
    }

    public Long getId() {
        return id;
    }
}
