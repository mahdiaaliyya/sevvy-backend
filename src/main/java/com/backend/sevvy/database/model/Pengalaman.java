package com.backend.sevvy.database.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table
public class Pengalaman implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;

    @Column(name = "posisi")
    private String posisi;

    @Column(name = "perusahaan",nullable = false)
    private String perusahaan;

    @Column(name = "periodeMulai", nullable = false)
    private int periodeMulai;

    @Column(name = "periodeSelesai")
    private int periodeSelesai;

    @Column(name = "deskripsi")
    private String deskripsi;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(referencedColumnName = "email", updatable = false)
    private Users users;

    public Pengalaman(){

    }

    public Pengalaman(Long id, String posisi, String perusahaan,
                      int periodeMulai, int periodeSelesai, String deskripsi) {
        this.id = id;
        this.posisi = posisi;
        this.perusahaan = perusahaan;
        this.periodeMulai = periodeMulai;
        this.periodeSelesai = periodeSelesai;
        this.deskripsi = deskripsi;
    }

    public String getPosisi() {
        return posisi;
    }

    public void setPosisi(String posisi) {
        this.posisi = posisi;
    }

    public String getPerusahaan() {
        return perusahaan;
    }

    public void setPerusahaan(String perusahaan) {
        this.perusahaan = perusahaan;
    }

    public int getPeriodeMulai() {
        return periodeMulai;
    }

    public void setPeriodeMulai(int periodeMulai) {
        this.periodeMulai = periodeMulai;
    }

    public int getPeriodeSelesai() {
        return periodeSelesai;
    }

    public void setPeriodeSelesai(int periodeSelesai) {
        this.periodeSelesai = periodeSelesai;
    }

    public String getDeskripsi() {
        return deskripsi;
    }

    public void setDeskripsi(String deskripsi) {
        this.deskripsi = deskripsi;
    }

    public void setUsers(Users users) {
        this.users = users;
    }


    public Long getId() {
        return id;
    }
}
