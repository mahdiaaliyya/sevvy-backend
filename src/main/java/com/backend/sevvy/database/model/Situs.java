package com.backend.sevvy.database.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table
public class Situs implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;

    @Column(name = "label")
    private String label;

    @Column(name = "link")
    private String link;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(referencedColumnName = "email", updatable = false)
    private Users users;

    public Situs(){

    }

    public Situs(Long id, String label, String link) {
        this.id = id;
        this.label = label;
        this.link = link;
    }

    public String getLabel() {
        return label;
    }

    public String getLink() {
        return link;
    }

    public Situs setLabel(String label) {
        this.label = label;
        return this;
    }

    public Situs setLink(String link) {
        this.link = link;
        return this;
    }

    public Long getId() {
        return id;
    }

    public void setUsers(Users users) {
        this.users = users;
    }

}
