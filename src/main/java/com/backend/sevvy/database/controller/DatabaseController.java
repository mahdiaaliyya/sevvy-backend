package com.backend.sevvy.database.controller;

import com.backend.sevvy.database.model.CV;
import com.backend.sevvy.database.model.KegiatanSukarela;
import com.backend.sevvy.database.model.Keterampilan;
import com.backend.sevvy.database.model.Pencapaian;
import com.backend.sevvy.database.model.Pendidikan;
import com.backend.sevvy.database.model.Pengalaman;
import com.backend.sevvy.database.model.PersonalInfo;
import com.backend.sevvy.database.model.Situs;
import com.backend.sevvy.database.model.Users;
import com.backend.sevvy.database.service.SevvyService;
import java.io.IOException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping(path = "/database")
@CrossOrigin(origins = "*", allowCredentials = "true")
public class DatabaseController {
    @Autowired
    private final SevvyService sevvyService;

    public DatabaseController(SevvyService sevvyService) {
        this.sevvyService = sevvyService;
    }

    // USERS AND ACCOUNT
    @PostMapping("/register")
    public ResponseEntity createUser(@RequestBody Users user) {
        return ResponseEntity.ok(sevvyService.addUser(user));
    }

    @GetMapping("/users/all")
    public ResponseEntity getAllUsers() {
        return ResponseEntity.ok(sevvyService.getAllUser());
    }

    @GetMapping("/users")
    public ResponseEntity getUser(@RequestParam String email) {
        return ResponseEntity.ok(sevvyService.findUserByEmail(email));
    }

    //    PERSONAL INFO
    @PutMapping("/profile/personal")
    public ResponseEntity updatePersonalInfo(@RequestBody PersonalInfo personalInfo) {
        return ResponseEntity.ok(sevvyService.updatePersonalInfo(personalInfo));
    }

    @GetMapping("/profile/personal")
    public ResponseEntity getPersonalInfo(@RequestParam String email) {
        return ResponseEntity.ok(sevvyService.getPersonalInfo(email));
    }

    //    SITUS
    @PostMapping("/profile/situs/{email}")
    public ResponseEntity addSitus(@PathVariable(value = "email") String email,
                                   @RequestBody Situs situs) {
        return ResponseEntity.ok(sevvyService.addSitus(email, situs));
    }

    @GetMapping("/profile/situs/all")
    public ResponseEntity getAllSitus(@RequestParam String email) {
        return ResponseEntity.ok(sevvyService.findSitusByEmail(email));
    }

    @GetMapping("/profile/situs")
    public ResponseEntity getSitus(@RequestParam long id) {
        return ResponseEntity.ok(sevvyService.findSitusById(id));
    }

    @PutMapping("/profile/situs")
    public ResponseEntity updateSitus(@RequestBody Situs situs) {
        return ResponseEntity.ok(sevvyService.updateSitus(situs));
    }

    @DeleteMapping("/profile/situs")
    public ResponseEntity deleteSitus(@RequestParam long id) {
        sevvyService.deleteSitus(id);
        return ResponseEntity.ok("situs deleted");
    }

    //    PENDIDIKAN
    @PostMapping("/profile/pendidikan/{email}")
    public ResponseEntity addPendidikan(@PathVariable(value = "email") String email,
                                        @RequestBody Pendidikan pendidikan) {
        return ResponseEntity.ok(sevvyService.addPendidikan(email, pendidikan));
    }

    @GetMapping("/profile/pendidikan/all")
    public ResponseEntity getAllPendidikan(@RequestParam String email) {
        return ResponseEntity.ok(sevvyService.findPendidikanByEmail(email));
    }

    @GetMapping("/profile/pendidikan")
    public ResponseEntity getPendidikan(@RequestParam Long id) {
        return ResponseEntity.ok(sevvyService.findPendidikanById(id));
    }

    @PutMapping("/profile/pendidikan")
    public ResponseEntity findPendidikan(@RequestBody Pendidikan pendidikan) {
        return ResponseEntity.ok(sevvyService.updatePendidikan(pendidikan));
    }

    @DeleteMapping("/profile/pendidikan")
    public ResponseEntity deletePendidikan(@RequestParam Long id) {
        sevvyService.deletePendidikan(id);
        return ResponseEntity.ok("pendidikan deleted");
    }

    //    PENGALAMAN

    @PostMapping("/profile/pengalaman/{email}")
    public ResponseEntity createPengalaman(@PathVariable(value = "email") String email,
                                           @RequestBody Pengalaman pengalaman) {
        return ResponseEntity.ok(sevvyService.addPengalaman(email, pengalaman));
    }

    @GetMapping("/profile/pengalaman/all")
    public ResponseEntity getAllPengalaman(@RequestParam String email) {
        return ResponseEntity.ok(sevvyService.findPengalamanByEmail(email));
    }

    @GetMapping("/profile/pengalaman")
    public ResponseEntity getPengalaman(@RequestParam Long id) {
        return ResponseEntity.ok(sevvyService.findPengalamanById(id));
    }

    @PutMapping("/profile/pengalaman")
    public ResponseEntity findPengalaman(@RequestBody Pengalaman pengalaman) {
        return ResponseEntity.ok(sevvyService.updatePengalaman(pengalaman));
    }

    @DeleteMapping("/profile/pengalaman")
    public ResponseEntity deletePengalaman(@RequestParam Long id) {
        sevvyService.deletePengalaman(id);
        return ResponseEntity.ok("pengalaman deleted");
    }

    //    PENCAPAIAN

    @PostMapping("/profile/pencapaian/{email}")
    public ResponseEntity createPencapaian(@PathVariable(value = "email") String email,
                                           @RequestBody Pencapaian pencapaian) {
        return ResponseEntity.ok(sevvyService.addPencapaian(email, pencapaian));
    }

    @GetMapping("/profile/pencapaian/all")
    public ResponseEntity getAllPencapaian(@RequestParam String email) {
        return ResponseEntity.ok(sevvyService.findPencapaianByEmail(email));
    }

    @GetMapping("/profile/pencapaian")
    public ResponseEntity getPencapaian(@RequestParam Long id) {
        return ResponseEntity.ok(sevvyService.findPencapaianById(id));
    }

    @PutMapping("/profile/pencapaian")
    public ResponseEntity updatePencapaian(@RequestBody Pencapaian pencapaian) {
        return ResponseEntity.ok(sevvyService.updatePencapaian(pencapaian));
    }

    @DeleteMapping("/profile/pencapaian")
    public ResponseEntity deletePencapaian(@RequestParam Long id) {
        sevvyService.deletePencapaian(id);
        return ResponseEntity.ok("pencapaian deleted");
    }
    //    KEGIATANSUKARELA

    @PostMapping("/profile/kegiatan-sukarela/{email}")
    public ResponseEntity createKegiatanSukarela(@PathVariable(value = "email") String email,
                                                 @RequestBody KegiatanSukarela kegiatanSukarela) {
        return ResponseEntity.ok(sevvyService.addKegiatanSukarela(email, kegiatanSukarela));
    }

    @GetMapping("/profile/kegiatan-sukarela/all")
    public ResponseEntity getAllKegiatanSukarela(@RequestParam String email) {
        return ResponseEntity.ok(sevvyService.findKegiatanSukarelaByEmail(email));
    }

    @GetMapping("/profile/kegiatan-sukarela")
    public ResponseEntity getKegiatanSukarela(@RequestParam Long id) {
        return ResponseEntity.ok(sevvyService.findKegiatanSukarelaById(id));
    }

    @PutMapping("/profile/kegiatan-sukarela")
    public ResponseEntity updateKegiatanSukarela(@RequestBody KegiatanSukarela kegiatanSukarela) {
        return ResponseEntity.ok(sevvyService.updateKegiatanSukarela(kegiatanSukarela));
    }

    @DeleteMapping("/profile/kegiatan-sukarela")
    public ResponseEntity deleteKegiatanSukarela(@RequestParam Long id) {
        sevvyService.deleteKegiatanSukarela(id);
        return ResponseEntity.ok("kegiatan deleted");
    }

    //    KETERAMPILAN

    @PostMapping("/profile/keterampilan/{email}")
    public ResponseEntity createKeterampilan(@PathVariable(value = "email") String email,
                                             @RequestBody Keterampilan keterampilan) {
        return ResponseEntity.ok(sevvyService.addKeterampilan(email, keterampilan));
    }

    @GetMapping("/profile/keterampilan/all")
    public ResponseEntity getAllKeterampilan(@RequestParam String email) {
        return ResponseEntity.ok(sevvyService.findKeterampilanByEmail(email));
    }

    @GetMapping("/profile/keterampilan")
    public ResponseEntity getKeterampilan(@RequestParam Long id) {
        return ResponseEntity.ok(sevvyService.findKeterampilanById(id));
    }

    @PutMapping("/profile/keterampilan")
    public ResponseEntity updateKeterampilan(@RequestBody Keterampilan keterampilan) {
        return ResponseEntity.ok(sevvyService.updateKeterampilan(keterampilan));
    }

    @DeleteMapping("/profile/keterampilan")
    public ResponseEntity deleteKeterampilan(@RequestParam Long id) {
        sevvyService.deleteKeterampilan(id);
        return ResponseEntity.ok("keterampilan deleted");
    }

    @PostMapping("/cv/{email}")
    public ResponseEntity addCV(@PathVariable(value = "email") String email,
                                   @RequestBody CV cv) {
        return ResponseEntity.ok(sevvyService.addCV(email, cv));
    }

    @GetMapping("/cv/all")
    public ResponseEntity getAllCV(@RequestParam String email) {
        return ResponseEntity.ok(sevvyService.findCVByEmail(email));
    }

    @GetMapping("/cv")
    public ResponseEntity getCV(@RequestParam long id) {
        return ResponseEntity.ok(sevvyService.findCVById(id));
    }

    @PutMapping("/cv")
    public ResponseEntity updatecv(@RequestBody CV cv) {
        return ResponseEntity.ok(sevvyService.updateCV(cv));
    }

    @DeleteMapping("/cv")
    public ResponseEntity deleteCV(@RequestParam long id) {
        sevvyService.deleteCV(id);
        return ResponseEntity.ok("cv deleted");
    }


    @GetMapping("/pdf/modern")
    public ResponseEntity getPdfModern(@RequestParam String email, @RequestParam int colorCode,
                                       @RequestParam String cvName, @RequestParam String cvLang)
            throws IOException {
        byte[] contents = sevvyService.downloadPDF(sevvyService.findUserByEmail(email), "MODERN",
                colorCode, cvLang);
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_PDF);
        String filename = cvName + ".pdf";
        headers.setContentDispositionFormData(filename, filename);
        headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
        ResponseEntity<byte[]> response = new ResponseEntity<>(contents, headers, HttpStatus.OK);
        return response;
    }

    @GetMapping("/pdf/formal")
    public ResponseEntity getPdfFormal(@RequestParam String email, @RequestParam int colorCode,
                                       @RequestParam String cvName, @RequestParam String cvLang)
            throws IOException {
        byte[] contents = sevvyService.downloadPDF(sevvyService.findUserByEmail(email),
                "FORMAL", colorCode, cvLang);
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_PDF);
        String filename = cvName + ".pdf";
        headers.setContentDispositionFormData(filename, filename);
        headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
        ResponseEntity<byte[]> response = new ResponseEntity<>(contents, headers, HttpStatus.OK);
        return response;
    }
}

