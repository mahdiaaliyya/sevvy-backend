package com.backend.sevvy.database.repository;

import com.backend.sevvy.database.model.Pengalaman;
import com.backend.sevvy.database.model.Users;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;



@Repository
public interface PengalamanRepository extends JpaRepository<Pengalaman,String> {
    List<Pengalaman> findAll();

    List<Pengalaman> findPengalamanByUsers(Users users);

    Pengalaman findPengalamanById(Long id);

    Optional<Pengalaman> findById(Long id);

    @Transactional
    void deleteById(Long id);
}
