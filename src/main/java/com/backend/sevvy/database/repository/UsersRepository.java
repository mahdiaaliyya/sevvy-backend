package com.backend.sevvy.database.repository;

import com.backend.sevvy.database.model.Users;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;



@Repository
public interface UsersRepository extends JpaRepository<Users,String> {
    List<Users> findAll();

    Users findUserByEmail(String email);
}
