package com.backend.sevvy.database.repository;

import com.backend.sevvy.database.model.Situs;
import com.backend.sevvy.database.model.Users;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;


@Repository
public interface SitusRepository extends JpaRepository<Situs,String> {
    List<Situs> findAll();

    List<Situs> findSitusByUsers(Users users);

    Situs findSitusById(Long id);

    @Transactional
    void deleteById(Long id);
}
