package com.backend.sevvy.database.repository;

import com.backend.sevvy.database.model.Pencapaian;
import com.backend.sevvy.database.model.Users;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;


@Repository
public interface PencapaianRepository extends JpaRepository<Pencapaian,String> {
    List<Pencapaian> findAll();

    List<Pencapaian> findPencapaianByUsers(Users users);

    Pencapaian findPencapaianById(Long id);

    @Transactional
    void deleteById(Long id);
}
