package com.backend.sevvy.database.repository;

import com.backend.sevvy.database.model.PersonalInfo;
import com.backend.sevvy.database.model.Users;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface PersonalInfoRepository extends JpaRepository<PersonalInfo,String> {
    List<PersonalInfo> findAll();

    List<PersonalInfo> findByUsers(Users users);

}
