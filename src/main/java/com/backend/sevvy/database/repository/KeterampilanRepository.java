package com.backend.sevvy.database.repository;

import com.backend.sevvy.database.model.Keterampilan;
import com.backend.sevvy.database.model.Users;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;





@Repository
public interface KeterampilanRepository extends JpaRepository<Keterampilan,String> {
    List<Keterampilan> findAll();

    List<Keterampilan> findKeterampilanByUsers(Users users);

    Keterampilan findKeterampilanById(Long id);

    @Transactional
    void deleteById(Long id);
}
