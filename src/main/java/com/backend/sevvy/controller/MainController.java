package com.backend.sevvy.controller;

import com.backend.sevvy.database.service.SevvyService;

import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class MainController {

    @Autowired
    private SevvyService sevvyService;

    @RequestMapping(method = RequestMethod.GET, value = "/")
    @ResponseBody
    public void handleGet(HttpServletResponse response) {
        response.setHeader("Location", "localhost:3000/");
        response.setStatus(302);
    }

    @GetMapping("/restricted")
    public String restricted() {
        return "You need to be logged in";
    }

}

