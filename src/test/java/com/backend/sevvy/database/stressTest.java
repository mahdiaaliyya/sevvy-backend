//package com.backend.sevvy.database;
//
//import com.backend.sevvy.database.model.KegiatanSukarela;
//import com.backend.sevvy.database.model.Keterampilan;
//import com.backend.sevvy.database.model.Pencapaian;
//import com.backend.sevvy.database.model.Pendidikan;
//import com.backend.sevvy.database.model.Pengalaman;
//import com.backend.sevvy.database.model.PersonalInfo;
//import com.backend.sevvy.database.model.Situs;
//import com.backend.sevvy.database.model.Users;
//import com.backend.sevvy.database.repository.KegiatanSukarelaRepository;
//import com.backend.sevvy.database.repository.KeterampilanRepository;
//import com.backend.sevvy.database.repository.PencapaianRepository;
//import com.backend.sevvy.database.repository.PendidikanRepository;
//import com.backend.sevvy.database.repository.PengalamanRepository;
//import com.backend.sevvy.database.repository.PersonalInfoRepository;
//import com.backend.sevvy.database.repository.SitusRepository;
//import com.backend.sevvy.database.repository.UsersRepository;
//import com.backend.sevvy.database.service.SevvyServiceImpl;
//import org.junit.Before;
//import org.junit.jupiter.api.Test;
//import org.junit.jupiter.api.extension.ExtendWith;
//import org.mockito.InjectMocks;
//import org.mockito.Mock;
//
//import static org.mockito.Mockito.times;
//import static org.mockito.Mockito.verify;
//
//import org.mockito.junit.jupiter.MockitoExtension;
//
//public class stressTest {
//    @Mock
//    private UsersRepository usersRepository;
//    @Mock
//    private PersonalInfoRepository personalInfoRepository;
//    @Mock
//    private PencapaianRepository pencapaianRepository;
//    @Mock
//    private PendidikanRepository pendidikanRepository;
//    @Mock
//    private KeterampilanRepository keterampilanRepository;
//    @Mock
//    private KegiatanSukarelaRepository kegiatanSukarelaRepository;
//    @Mock
//    private PengalamanRepository pengalamanRepository;
//    @Mock
//    private SitusRepository situsRepository;
//    @InjectMocks
//    private SevvyServiceImpl sevvyService;
//
//    Users users;
//    Situs situs;
//    Long id;
//    PersonalInfo personalInfo;
//    Pengalaman pengalaman;
//    Pendidikan pendidikan;
//    Pencapaian pencapaian;
//    Keterampilan keterampilan;
//    KegiatanSukarela kegiatanSukarela;
//
//    @Before
//    public void setUp() {
//        users = new Users("dum@mail.com", "pw");
//    }
//
////    @Test
////    public void testAddUsers() {
////        Users users = new Users("dum@mail.com", "pw");
////        sevvyService.addUser(users);
////
////        verify(usersRepository, times(1)).save(users);
////    }
//
////    @Test
////    public void testgetAll() {
////        sevvyService.getAllUser();
////        sevvyService.getAllKegiatanSukarela();
////        sevvyService.getAllKeterampilan();
////        sevvyService.getAllPencapaian();
////        sevvyService.getAllPendidikan();
////        sevvyService.getAllPengalaman();
////        sevvyService.getAllPersonalInfo();
////        sevvyService.getAllSitus();
////
////        verify(usersRepository, times(1)).findAll();
////        verify(kegiatanSukarelaRepository, times(1)).findAll();
////        verify(keterampilanRepository, times(1)).findAll();
////        verify(pencapaianRepository, times(1)).findAll();
////        verify(pendidikanRepository, times(1)).findAll();
////        verify(pengalamanRepository, times(1)).findAll();
////        verify(personalInfoRepository, times(1)).findAll();
////        verify(situsRepository, times(1)).findAll();
////    }
//
//    @Test
//    public void testAddProfile() {
//        int interationId = 1;
//        users = new Users("dum@mail.com", "pw");
//
//        for (int iteration=0;iteration<1000;iteration++){
//
//            kegiatanSukarela = new KegiatanSukarela((long) interationId, "a","a",2020,2020,"");
//            keterampilan = new Keterampilan((long) interationId, "a",9);
//            pencapaian = new Pencapaian();
//            pengalaman = new Pengalaman();
//            pendidikan = new Pendidikan();
//            situs = new Situs();
//
//            sevvyService.addUser(users);
//            sevvyService.addKegiatanSukarela("dum@mail.com", kegiatanSukarela);
//            sevvyService.addKeterampilan("dum@mail.com", keterampilan);
//            sevvyService.addPencapaian("dum@mail.com", pencapaian);
//            sevvyService.addPengalaman("dum@mail.com", pengalaman);
//            sevvyService.addPendidikan("dum@mail.com",pendidikan);
//            sevvyService.addSitus("dum@mail.com", situs);
//        }
//
//
//
//        verify(kegiatanSukarelaRepository, times(1)).save(kegiatanSukarela);
//        verify(situsRepository, times(1)).save(situs);
//        verify(pengalamanRepository, times(1)).save(pengalaman);
//        verify(keterampilanRepository, times(1)).save(keterampilan);
//        verify(pencapaianRepository, times(1)).save(pencapaian);
//        verify(pendidikanRepository, times(1)).save(pendidikan);
//    }
//
////    @Test
////    public void testUpdateSitus() {
////        users = new Users("dum@mail.com", "pw");
////        users = usersRepository.save(users);
////        situs = new Situs((long)1, "label", "link");
////        sevvyService.addSitus("dum@mail.com", situs);
////        situsRepository.save(situs);
////        id =  situs.getId();
////
////        situs = new Situs((long)1, "label2", "link");
////        sevvyService.updateSitus(situs);
//////        assertEquals("label2",sevvyService.findSitusById((long)1).getLabel());
////        verify(situsRepository, times(1)).save(situs);
////    }
////
////    @Test
////    public void testUpdatePersonalInfo() {
////        users = usersRepository.save(users);
////        personalInfo = new PersonalInfo("", "","dum@mail.com","","","","","","");
////        personalInfo.setUsers(users);
////        personalInfoRepository.save(personalInfo);
////
////        personalInfo = new PersonalInfo("a", "a","dum@mail.com","","","","","","");
////        sevvyService.updatePersonalInfo(personalInfo);
////        verify(personalInfoRepository, times(1)).save(personalInfo);
////    }
////
////    @Test
////    public void testDeleteProfile() {
////        sevvyService.deleteSitus(id);
////    }
////
////    @Test
////    public void testFindProfileByEmail() {
////
////        Users users = new Users("dum@mail.com", "pw");
////        sevvyService.findKegiatanSukarelaByEmail("dum@mail.com");
////        sevvyService.findKeterampilanByEmail("dum@mail.com");
////        sevvyService.findPencapaianByEmail("dum@mail.com");
////        sevvyService.findPendidikanByEmail("dum@mail.com");
////        sevvyService.findPengalamanByEmail("dum@mail.com");
////        sevvyService.findSitusByEmail("dum@mail.com");
////
////        Users user = sevvyService.findUserByEmail("dum@mail.com");
////        verify(kegiatanSukarelaRepository, times(1)).findKegiatanSukarelaByUsers(user);
////        verify(keterampilanRepository, times(1)).findKeterampilanByUsers(user);
////        verify(pencapaianRepository, times(1)).findPencapaianByUsers(user);
////        verify(pendidikanRepository, times(1)).findPendidikanByUsers(user);
////        verify(pengalamanRepository, times(1)).findPengalamanByUsers(user);
////        verify(situsRepository, times(1)).findSitusByUsers(user);
////    }
////
////    @Test
////    public void testFindProfileById() {
////        sevvyService.findKegiatanSukarelaById((long)1);
////        sevvyService.findKeterampilanById((long)2);
////        sevvyService.findPencapaianById((long)3);
////        sevvyService.findPendidikanById((long)4);
////        sevvyService.findPengalamanById((long)5);
////        sevvyService.findSitusById((long)6);
////
////        verify(kegiatanSukarelaRepository, times(1)).findKegiatanSukarelaById((long)1);
////        verify(keterampilanRepository, times(1)).findKeterampilanById((long)2);
////        verify(pencapaianRepository, times(1)).findPencapaianById((long)3);
////        verify(pendidikanRepository, times(1)).findPendidikanById((long)4);
////        verify(pengalamanRepository, times(1)).findPengalamanById((long)5);
////        verify(situsRepository, times(1)).findSitusById((long)6);
////    }
//
//}
