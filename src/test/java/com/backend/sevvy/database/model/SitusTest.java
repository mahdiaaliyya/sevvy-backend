package com.backend.sevvy.database.model;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class SitusTest {
    Situs situs = new Situs();

    @BeforeEach
    public void setUp() {
        situs = new Situs((long)1, "linkedin","linked.in/nasywa");
    }

    @Test
    public void testGetter() {
        assertEquals(1, situs.getId());
        assertEquals("linkedin",situs.getLabel());
        assertEquals("linked.in/nasywa",situs.getLink());
    }

    @Test
    public void testSetter() {
        situs.setLabel("ig");
        situs.setLink("nasy.nf");
        assertEquals("ig", situs.getLabel());
        assertEquals("nasy.nf",situs.getLink());
    }
}
