package com.backend.sevvy.database.model;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class PengalamanTest {
    Pengalaman pengalaman = new Pengalaman();

    @BeforeEach
    public void setUp() {
        pengalaman = new Pengalaman((long)1, "Direktur","Holcim",2016,2018,"test");
    }

    @Test
    public void testGetter() {
        assertEquals((long)1, pengalaman.getId());
        assertEquals("Direktur",pengalaman.getPosisi());
        assertEquals("Holcim",pengalaman.getPerusahaan());
        assertEquals(2016,pengalaman.getPeriodeMulai());
        assertEquals(2018,pengalaman.getPeriodeSelesai());
        assertEquals("test",pengalaman.getDeskripsi());
    }

    @Test
    public void testSetter() {
        pengalaman.setPosisi("intern");
        pengalaman.setPerusahaan("abc");
        pengalaman.setPeriodeMulai(2019);
        pengalaman.setPeriodeSelesai(2020);
        pengalaman.setDeskripsi("test2");
        assertEquals("intern",pengalaman.getPosisi());
        assertEquals("abc",pengalaman.getPerusahaan());
        assertEquals(2019,pengalaman.getPeriodeMulai());
        assertEquals(2020,pengalaman.getPeriodeSelesai());
        assertEquals("test2",pengalaman.getDeskripsi());
    }

}
