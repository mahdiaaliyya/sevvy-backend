package com.backend.sevvy.database.model;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


public class PendidikanTest {
    Pendidikan pendidikan = new Pendidikan();

    @BeforeEach
    public void setUp() {
        pendidikan = new Pendidikan((long)1, "SD", "SAB", "", 2020, 2020);
    }

    @Test
    public void testJenjangGetter() {
        assertEquals("SD", pendidikan.getJenjang());
    }

    @Test
    public void testNamaInstitusiGetter() {
        assertEquals("SAB", pendidikan.getNamaInstitusi());
    }

    @Test
    public void testIdGetter() {
        assertEquals((long)1, pendidikan.getId());
    }

    @Test
    public void testJenjangSetter() {
        pendidikan.setJenjang("SMP");
        assertEquals("SMP", pendidikan.getJenjang());
    }

    @Test
    public void testNamaInstitusiSetter() {
        pendidikan.setNamaInstitusi("Al-Azhar");
        assertEquals("Al-Azhar", pendidikan.getNamaInstitusi());
    }
}
