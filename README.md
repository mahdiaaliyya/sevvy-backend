# Sevvy


[![pipeline status](https://gitlab.com/Sevvy/sevvy-backend/badges/master/pipeline.svg)](https://gitlab.com/Sevvy/sevvy-backend/-/commits/master)
[![coverage report](https://gitlab.com/Sevvy/sevvy-backend/badges/master/coverage.svg)](https://gitlab.com/Sevvy/sevvy-backend/-/commits/master)

Group Members:
- Mahdia Aliyya Nuha Kiswanto   : 1806141290
- Nasywa Nur Fathiyah           : 1806205546
- Hadlina Rahmadinni            : 1606918566
- Fauzan Deni Rywannis          : 1606881355

Link Heroku:<br/>
main-app    : [sevvy.herokuapp.com](sevvy.herokuapp.com)<br/>
database    : [sevvydb.herokuapp.com](sevvydb.herokuapp.com)
  

